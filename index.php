<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<title>Desu, Ltd.</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<h1>desu.ltd?</h1>
			<p>desu.ltd. The domain of me, a guy on the internet.</p>
			<p>There's not much here, at least that you should care about.</p>
			<h1>Docs</h1>
			<ul>
				<li><a href="/about">/about</a> - A page with some info about me</li>
				<li><a href="/ascii">/ascii</a> - ASCII art I've collected over time</li>
			</ul>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
