<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>414 URI Too Long</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<pre>
                                                                          
  _____      _____   ___   ___   ___   ___   ___   ___   ___   ___   ___  
 / _ \ \ /\ / / _ \ / _ \ / _ \ / _ \ / _ \ / _ \ / _ \ / _ \ / _ \ / _ \ 
| (_) \ V  V / (_) | (_) | (_) | (_) | (_) | (_) | (_) | (_) | (_) | (_) |
 \___/ \_/\_/ \___/ \___/ \___/ \___/ \___/ \___/ \___/ \___/ \___/ \___/ 
				</pre>
				<h1>414</h1>
				<p>The request you sent had a URI that was waaaaayy too long.</p>
				<p>I mean just look at the URL in your URL bar. It's huge.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
