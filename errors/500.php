<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>500 Internal Server Error</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<pre>
   ____            ____  
  / __ \__      __/ __ \ 
 / / _` \ \ /\ / / / _` |
| | (_| |\ V  V / | (_| |
 \ \__,_| \_/\_/ \ \__,_|
  \____/          \____/ 
				</pre>
				<h1>500</h1>
				<p>Something bad happened. Contact the site administrator and mention the error "500", the URL you tried to access, and the time it happened.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
