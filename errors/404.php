<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<link href="/errors/styles/styles.css" type="text/css" rel="stylesheet" />
		<title>404 Not Found</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<pre>
                   
  _____      _____ 
 / _ \ \ /\ / / _ \
|  __/\ V  V /  __/
 \___| \_/\_/ \___|
				</pre>
				<h1>404</h1>
				<p>Yeah, we couldn't find that one. Sorry.</p>
				<p>Try your request again and, if it keeps happening, contact the site administrator.</p>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
