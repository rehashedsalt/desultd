<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<title>About - Desu, Ltd.</title>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>About desu.ltd</h1>
				<p>desu.ltd is free and open-source software (to the degree that these documents can be considered software). It hosts its own source under a git repo at <a href="https://git.desu.ltd/salt/desultd">git.desu.ltd/salt/desultd</a>.</p>
				<p>While the repo may say different, if you're reading this text, you can use any content within this site that I hold rights to under the terms of the Creative Commons CC0 license. I really don't give a shit.</p>
				<h1>Contact Information</h1>
				<p>Maybe don't. If you're so inclined, my email is attached to my commits.</p>
				</ul>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
