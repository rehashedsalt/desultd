<!DOCTYPE html>
<html>
	<head>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-meta.php';?>
		<title>ASCII Art - Desu, Ltd.</title>
		<style>.content pre {font-size:60%;text-align:center;margin:1em;padding:1em;background:rgba(0,0,0,0.10);}</style>
	</head>
	<body>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-header.php';?>
		<div class="content">
			<div class="section">
				<h1>ASCII Art</h1>
				<p>Where known, artists are attributed. Sadly, I don't know the artists for most of these.</p>
				<p>You can link to a specific section or piece by appending <code>#name</code> to the URL. So to link someone to the JoJo section, you'd link to <code>desu.ltd/ascii#jojo</code>.</p>
				<?php
					$dir = new DirectoryIterator(".");
					foreach ($dir as $fileinfo) {
						if (!$fileinfo->isDot() && $fileinfo->isDir()) {
							printf("<h2 id=%s>%s</h2>", $fileinfo->getFilename(), $fileinfo->getFilename());
							$dir2 = new DirectoryIterator($fileinfo->getFilename());
							foreach ($dir2 as $fileinfo2) {
								if (!$fileinfo2->isDot()) {
									printf("<pre id=%s style=\"display:inline-block;vertical-align:top;\">%s</pre>", $fileinfo2->getFilename(), file_get_contents($fileinfo2->getPathname()));
								}
							}
						}
					}
				?>
			</div>
		</div>
		<?php include $_SERVER['DOCUMENT_ROOT'].'/src/common-footer.php';?>
	</body>
</html>
